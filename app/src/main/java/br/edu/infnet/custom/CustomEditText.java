package br.edu.infnet.custom;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatMultiAutoCompleteTextView;
import android.util.AttributeSet;

/**
 * Created by raphael.bernardo on 16/02/2017.
 */

@CoordinatorLayout.DefaultBehavior(CustomMoveUpBehavior.class)
public class CustomEditText extends AppCompatMultiAutoCompleteTextView {
    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
