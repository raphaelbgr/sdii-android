package br.edu.infnet.custom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import br.edu.infnet.api.model.Client;
import br.edu.infnet.api.model.Message;
import br.edu.infnet.infnet.BuildConfig;
import io.paperdb.Paper;

/**
 * Created by raphael.bernardo on 20/02/2017.
 */

public class CustomCache {

    private static final String SERVER_STRING = "Servidor";

    public static String getDefaultEndpoint() {
        return Paper.book("server_data").read("defaultEndpoint");
    }

    public static void storeDefaultEndpoint(String endpoint) {
        Paper.book("server_data").write("defaultEndpoint", endpoint);
    }

    public static String getDefaultPort() {
        return Paper.book("server_data").read("defaultPort");
    }

    public static void storeDefaultPort(String port) {
        Paper.book("server_data").write("defaultPort", port);
    }

    public static String getDefaultUsername() {
        return Paper.book().read("defaultUsername");
    }

    public static void storeDefaultUsername(String userName) {
        Paper.book().write("defaultUsername", userName);
    }

    public static void destroyAll() {
        Paper.book().destroy();
    }

    public static void storeChatIds(HashMap<String, Integer> chatIds) {
        Paper.book().write("vChatIds", chatIds);
    }

    public static HashMap<String, Integer> getChatIds() {
        HashMap<String, Integer> vIds = Paper.book().read("vChatIds");
        if (vIds == null)
            return new  HashMap<String, Integer>();
        else
            return vIds;
    }

    public static List<String> getKnownUsers() {
        List<String> clients;
        try {
            clients = Paper.book().read("knownUsers");

            if (clients != null) {
                List<String> temp = new LinkedList<>(clients);
                Set<String> hashSet = new HashSet<>(temp);
                String [] temp2 = hashSet.toArray(new String[hashSet.size()]);
                clients = Arrays.asList(temp2);
            }
        } catch (Exception e) {
            e.printStackTrace();
            clients = null;
        }

        if (clients != null)
                return clients;
        else {
            clients = new ArrayList<>();

            // Adiciona o chat geral com servidor
            clients.add("0");
            return clients;
        }
    }

    public static void updateKnownUsers(List<String> clients) {
        Paper.book().write("knownUsers", clients);
    }

    public static List<Message> getMessageHistory() {
        List<Message> mList = Paper.book().read("messageLog");
        if (mList == null) {
            mList = new ArrayList<>();
        } else {
            mList = new ArrayList<>(mList);
        }

        mList = removeDuplicates(mList);
        return mList;
    }

    private static List<Message> removeDuplicates(List<Message> mList) {
        List<Message> uniques = new ArrayList<Message>();
        for (Message element : mList) {
            if (!uniques.contains(element)) {
                uniques.add(element);
            }
        }
        return uniques;
    }

    public static void updateMessageHistory(List<Message> list) {
        Paper.book().write("messageLog", list);
    }

    public static void addMessagesToCache(List<Message> data) {
        if (data != null && data.size() > 0) {
            List<Message> list = Paper.book().read("messageLog");
            if (list == null)
                list = new ArrayList<>();
            else
                list = new ArrayList<>(list);
            list.addAll(data);
            Paper.book().write("messageLog", list);
        }
    }

    public static void addSingleMessageToCache(Message data) {
        List<Message> list = Paper.book().read("messageLog");
        if (list != null)
            list = new ArrayList<>(list);
        else {
            list = new ArrayList<>();
        }

        list.add(data);
        Paper.book().write("messageLog", list);
    }
}
