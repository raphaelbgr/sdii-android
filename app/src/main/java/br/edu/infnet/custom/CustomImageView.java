package br.edu.infnet.custom;

/**
 * Created by raphael.bernardo on 16/02/2017.
 */


import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

@CoordinatorLayout.DefaultBehavior(CustomMoveUpBehavior.class)
public class CustomImageView extends AppCompatImageView {
    public CustomImageView(Context context) {
        super(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
