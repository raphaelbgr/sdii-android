package br.edu.infnet;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import br.edu.infnet.api.Api;
import io.paperdb.Paper;

/**
 * Created by raphael.bernardo on 16/02/2017.
 */

public class App extends Application {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Paper.init(this);

//        Api.getInstance().getServerHistory();
    }

}
