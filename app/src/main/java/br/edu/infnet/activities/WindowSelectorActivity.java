package br.edu.infnet.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.edu.infnet.api.Api;
import br.edu.infnet.api.interfaces.OnServerReceive;
import br.edu.infnet.api.model.Message;
import br.edu.infnet.api.model.Requisicao;
import br.edu.infnet.api.model.Resposta;
import br.edu.infnet.custom.CustomCache;
import br.edu.infnet.infnet.R;
import butterknife.ButterKnife;

/**
 * Created by raphael.bernardo on 20/03/2017.
 */

public class WindowSelectorActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnServerReceive {

    private static final String SERVER_NAME = "Servidor (Canal Geral)";
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ContactsAdapter mAdapter;
    private NavigationView navigationView;
    private FloatingActionButton mFloatingAddButton;
    private FloatingActionButton mFloatingRefreshButton;
    private final String SERVER_ID = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_window_select);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_window_selector);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mRecyclerView = (RecyclerView) findViewById(R.id.contacts_recycle_view);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        // specify an adapter (see also next example)
        mAdapter = new ContactsAdapter(CustomCache.getKnownUsers());
        mRecyclerView.setAdapter(mAdapter);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mFloatingAddButton = (android.support.design.widget.FloatingActionButton) findViewById(R.id.fab_plus);
        mFloatingAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddDialog("Adicionar usuário", "Digite o usuário desejado:");
            }
        });

        mFloatingRefreshButton = (android.support.design.widget.FloatingActionButton) findViewById(R.id.fab_refresh);
        mFloatingRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Recarregando...", Toast.LENGTH_SHORT).show();
                Api.getInstance().getServerHistory();
            }
        });
    }

    public void showAddDialog(String title, String text){

        AlertDialog.Builder message = new AlertDialog.Builder(WindowSelectorActivity.this);
        message.setTitle(title);
        message.setMessage(text);
        final EditText input = new EditText(this);
        message.setView(input);
        message.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (input.getText() != null && !input.getText().toString().equalsIgnoreCase("")) {
                    List<String> newList = new ArrayList<>(mAdapter.getUserList());
                    newList.add(input.getText().toString());
                    mAdapter.setUserList(newList);
                    mAdapter.notifyDataSetChanged();
                    CustomCache.updateKnownUsers(newList);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Texto digitado inválido", Toast.LENGTH_SHORT).show();
                }
            }
        });

        message.show();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {
        private String userLastMessage;

        public void setUserList(List<String> userList) {this.userList = userList;}

        List<String> userList;

        public List<String> getUserList() {
            return userList;
        }

        public ContactsAdapter(List<String> userList) {this.userList = userList;}

//        public void setUserLastMessage(String userLastMessage) {
//            ViewHolder.get
//            this.userLastMessage = userLastMessage;
//        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public LinearLayout mContactCard;
            public TextView mContactLastMessage;
            public TextView mContactLastTimeStamp;
            public TextView mContactName;
            final String SERVER_NAME = "Servidor (Canal Geral)";

            public ViewHolder(View v) {
                super(v);

                mContactCard = (LinearLayout) v.findViewById(R.id.contact_card);
                mContactName = (TextView) v.findViewById(R.id.contact_username);
                mContactLastMessage = (TextView) v.findViewById(R.id.contact_last_message);
                mContactLastTimeStamp = (TextView) v.findViewById(R.id.timestamp);
            }
        }

        @Override
        public ContactsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.contact, parent, false);
            ViewHolder vh = new ViewHolder(v);

            return vh;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mContactName.setText(userList.get(position).equalsIgnoreCase("0") ?
                    holder.SERVER_NAME : userList.get(position));
//            holder.mContactLastMessage.setText("--");
//            holder.mContactLastTimeStamp.setText("--:--");

            holder.mContactCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    String selectedId = userList.get(position);

                    if (selectedId.equalsIgnoreCase(CustomCache.getDefaultUsername())) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Você não pode conversar consigo mesmo.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        if (selectedId.equalsIgnoreCase(holder.SERVER_NAME))
                            selectedId = "0";
                        b.putString("selectedId", selectedId);
                        Intent i = new Intent(WindowSelectorActivity.this, DirectMessagesActivity.class);
                        i.putExtras(b);
                        WindowSelectorActivity.this.startActivity(i);
                    }
                }
            });
        }

        @Override
        public int getItemCount() { return userList.size(); }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_chat) {}
        else if (id == R.id.nav_logout) {
            Api.getInstance().logoff();
            CustomCache.destroyAll();
            finish();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadCache();
            }
        }, 3300);
    }

    @Override
    public void onResume() {
        super.onResume();

        loginApi();
        setupData();
        Api.getInstance().getServerHistory();
    }

    private void loadCache() {
        List<Message> messageLog = CustomCache.getMessageHistory();

        if (messageLog != null) {
            Resposta r = new Resposta();
            r.setData(messageLog);

            onReceiveRespostaResponse(r);
        }
    }

    private void loginApi() {
        Api.getInstance().setLogin(CustomCache.getDefaultUsername());
        Api.getInstance().setReconnect(true);
        Api.getInstance().setListener(WindowSelectorActivity.this);
        Api.getInstance().setEndpoint(CustomCache.getDefaultEndpoint());
        Api.getInstance().setDefaultPort(CustomCache.getDefaultPort());
        Api.getInstance().loginAsync(CustomCache.getDefaultUsername(), null);
    }

    private void setupData() {
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.txt_username))
                .setText(CustomCache.getDefaultUsername());
    }

    @Override
    public void onAcceptSocket(Socket s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(WindowSelectorActivity.this.getApplicationContext(), "Conexão estabelecida!",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDataSend(final String data) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Toast.makeText(WindowSelectorActivity.this.getApplicationContext(), ">>>" + data,
//                        Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public void onReceiveData(final String data) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Toast.makeText(WindowSelectorActivity.this.getApplicationContext(), "<<< " + data,
//                        Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public void onSocketError(final Exception io) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(WindowSelectorActivity.this.getApplicationContext(),
                        io.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onReceiveRequisicaoResponse(Requisicao r) {
        try {
            if (r != null && r.getId() != null)
                Log.d("<<<", "Data Requisicao received from: " + r.getId());

            if (r != null && r.getId() != null && !r.getId().equalsIgnoreCase("0")
                    && !r.getId().equalsIgnoreCase(CustomCache.getDefaultUsername()))
                Api.getInstance().addKnownUser(r.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceiveRespostaResponse(final Resposta r) {
        try {
            if (r != null && r.getId() != null)
                Log.d("<<<", "Data Resposta received from: " + r.getId());

            if (r != null && r.getId() != null
                    && !r.getId().equalsIgnoreCase("0")
                    && !r.getId().equalsIgnoreCase(CustomCache.getDefaultUsername()))
                Api.getInstance().addKnownUser(r.getId());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int i = 0;
                    for (String recycleListName : mAdapter.getUserList()) {
                        View v = (View) mRecyclerView.getChildAt(i);

                        if (v != null) {
                            TextView txtMsg = (TextView) v.findViewById(R.id.contact_last_message);
                            TextView txtName = (TextView) v.findViewById(R.id.contact_username);
                            TextView txtDate = (TextView) v.findViewById(R.id.timestamp);

                            String contacIid = txtName.getText().toString();

                            if (r != null && r.getData() != null) {
                                List<Message> tempList = new ArrayList<Message>(r.getData());

                                for (Message m : tempList) {
                                    if (isForContact(contacIid.equalsIgnoreCase(SERVER_NAME) ? "0" : contacIid,
                                            recycleListName, m.getSrc())) {
                                        txtMsg.setText(m.getData());

                                        SimpleDateFormat sp = new SimpleDateFormat("HH:mm dd/MM/yy" );
                                        Date now = new Date();
                                        txtDate.setText(sp.format(now));
                                    }
                                }
                            }
                            i++;
                        }

                    }

                    CustomCache.addMessagesToCache(r.getData());
                }

                private boolean isForContact(String contacIid, String recycleListName, String src) {
                    if (contacIid != null && recycleListName != null)
                        return contacIid.equalsIgnoreCase(recycleListName) && contacIid.equalsIgnoreCase(src);
                    else return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceiveLogoffResponse() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(WindowSelectorActivity.this.getApplicationContext(), "Logoff feito com sucesso.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onReceiveMembersResponse(final List<String> list) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.setUserList(list);
                mAdapter.notifyDataSetChanged();
            }
        });
    }
}
