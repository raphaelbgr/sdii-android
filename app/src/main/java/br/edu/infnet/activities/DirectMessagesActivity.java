package br.edu.infnet.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ServiceCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import br.edu.infnet.api.Api;
import br.edu.infnet.api.interfaces.OnServerReceive;
import br.edu.infnet.api.model.Requisicao;
import br.edu.infnet.api.model.Resposta;
import br.edu.infnet.custom.CustomCache;
import br.edu.infnet.infnet.R;
import butterknife.ButterKnife;
import jp.bassaer.chatmessageview.models.Message;
import jp.bassaer.chatmessageview.models.User;
import jp.bassaer.chatmessageview.views.ChatView;

public class DirectMessagesActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnServerReceive {

    private User me;
    private Bitmap yourIcon;
    private int yourId;
    private HashMap<String, Integer> chatIds;
    private NavigationView navigationView;
    private ChatView mChatView;
    private String selectedId;
    private final String SERVER_ID = "0";
    private final String SERVER_CONTACT_NAME = "Servidor (Canal Geral)";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat_window);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            selectedId = getIntent().getExtras().getString("selectedId");
            if (selectedId == null)
                finish();
        } else
            finish();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (selectedId.equalsIgnoreCase(SERVER_ID))
            toolbar.setTitle(SERVER_CONTACT_NAME);
        else
            toolbar.setTitle(selectedId);

        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//        toggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mChatView = (ChatView)findViewById(R.id.chat_view);
        setupChatView();
    }

    private void setupChatView() {
        chatIds = CustomCache.getChatIds();

        //Set UI parameters if them need
        mChatView.setRightBubbleColor(ContextCompat.getColor(this, R.color.green500));
        mChatView.setLeftBubbleColor(Color.WHITE);
        mChatView.setBackgroundColor(ContextCompat.getColor(this, R.color.blueGray500));
        mChatView.setSendButtonColor(ContextCompat.getColor(this, R.color.colorPrimary));
        mChatView.setSendIcon(R.drawable.ic_action_send);
        mChatView.setRightMessageTextColor(Color.WHITE);
        mChatView.setLeftMessageTextColor(Color.BLACK);
        mChatView.setUsernameTextColor(Color.WHITE);
        mChatView.setSendTimeTextColor(Color.WHITE);
        mChatView.setDateSeparatorColor(Color.WHITE);
        mChatView.setInputTextHint("Escreva sua mensagem...");
        mChatView.setMessageMarginTop(5);
        mChatView.setMessageMarginBottom(5);

        //Click Send Button
        mChatView.setOnClickSendButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mChatView.getInputText() != null && !mChatView.getInputText().equalsIgnoreCase("")) {
                    Api.getInstance().sendMessage(mChatView.getInputText(), selectedId.equalsIgnoreCase("0") ? "0" : selectedId);

                    Message message = new Message.Builder()
                            .setUser(me)
                            .setRightMessage(true)
                            .setMessageText(mChatView.getInputText())
                            .hideIcon(true)
                            .build();

                    addSentMessageOnMessageView();
                    CustomCache.addSingleMessageToCache(
                            new br.edu.infnet.api.model.Message(CustomCache.getDefaultUsername(),
                                    message.getMessageText(), selectedId));
                } else {
                    Snackbar.make(view, "Não é permitido o envio de mensagens vazias.", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addSentMessageOnMessageView() {
        Message message = new Message.Builder()
                .setUser(me)
                .setRightMessage(true)
                .setMessageText(mChatView.getInputText())
                .hideIcon(true)
                .build();
        mChatView.send(message);
        mChatView.setInputText("");
    }

    private void addSentMessageOnMessageView(String src, String data) {
        final Message m = new Message.Builder()
                .setUser(me)
                .setRightMessage(true)
                .setMessageText(data)
                .hideIcon(true)
                .build();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mChatView.send(m);
            }
        });
    }

    private Integer getChatId(String src) {
        if (chatIds.get(src) == null)
            chatIds.put(src, new Random().nextInt());
        CustomCache.storeChatIds(chatIds);
        return chatIds.get(src);
    }

    private void addReceivedMessageOnMessageView(String src, String data) {
        User them = new User(getChatId(src), src.equalsIgnoreCase("0") ? "Servidor" : src, yourIcon);
        final Message m = new Message.Builder()
                .setUser(them)
                .setRightMessage(false)
                .setMessageText(data)
                .hideIcon(false)
                .build();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mChatView != null)
                    mChatView.receive(m);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Api.getInstance().setListener(DirectMessagesActivity.this);

//        receiveApi();
        setupData();
//        new Handler().post(new Runnable() {
//            @Override
//            public void run() {
//                Api.getInstance().getServerHistory();
//            }
//        });
//        Api.getInstance().getServerHistory();
        loadCache();
    }

//    private void receiveApi() {
//          Api.getInstance().getServerHistory();
//    }

    private void setupData() {
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.txt_username))
                .setText(CustomCache.getDefaultUsername());

        yourId = 1;
        yourIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_user);

        int myId = 0;
        Bitmap myIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_user);
        String myName = CustomCache.getDefaultUsername();
        me = new User(myId, myName, myIcon);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_chat) {}
        else if (id == R.id.nav_logout) {
            Api.getInstance().logoff();
            CustomCache.destroyAll();
            finish();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onAcceptSocket(Socket s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Conexão reestabelecida.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDataSend(final String data) {}

    @Override
    public void onReceiveData(final String data) {}

    @Override
    public void onSocketError(final Exception io) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), io.getLocalizedMessage(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onReceiveRequisicaoResponse(final Requisicao resp) {
        if (!resp.getData().equalsIgnoreCase("")
                && !resp.getId().equalsIgnoreCase(CustomCache.getDefaultUsername())) {

            if (selectedId.equalsIgnoreCase(resp.getDst()))
                addReceivedMessageOnMessageView(resp.getId(), resp.getData());
        }
    }

    @Override
    public void onReceiveRespostaResponse(final Resposta resp) {
        if (resp != null && resp.getId() != null)
            Log.d("<<<", "Data Resposta received from: " + resp.getId());

        if (resp.getData() != null) {
            for (br.edu.infnet.api.model.Message m : resp.getData()) {
                if (isForAll(m))
                    sendOrReceive(m);
                else if (isForCurrentWindow(m))
                    sendOrReceive(m);

                if (m.getData() != null && !m.getData().equalsIgnoreCase(""))
                    CustomCache.addSingleMessageToCache(m);
            }
        }
    }

    private boolean isForCurrentWindow(br.edu.infnet.api.model.Message m) {
        // SERVIDOR DO PROFESSOR MANDA DST == NULL
        return (m != null && m.getSrc() != null || m.getDst() != null)
                && ((selectedId.equalsIgnoreCase(m.getDst())
                || selectedId.equalsIgnoreCase(m.getSrc())));
    }

    private boolean isForAll(br.edu.infnet.api.model.Message m) {
        // BUG DO SERVIDOR DO PROFESSOR, NÃO ENVIA O DESTINO
        return m != null && m.getDst() != null && ((m.getDst().equalsIgnoreCase(selectedId)
                && selectedId.equalsIgnoreCase(SERVER_ID)));
    }

    private void sendOrReceive(br.edu.infnet.api.model.Message m) {
        if (m.getData() != null && !m.getData().equalsIgnoreCase("") && isFromSelf(m))
            addSentMessageOnMessageView(m.getSrc(), m.getData());
        else if (m.getData() != null && !m.getData().equalsIgnoreCase("") && isForSelf(m))
            addReceivedMessageOnMessageView(m.getSrc(), m.getData());
    }

    private boolean isFromSelf(br.edu.infnet.api.model.Message m) {
        return m.getSrc().equalsIgnoreCase(CustomCache.getDefaultUsername());
    }

    private boolean isForSelf(br.edu.infnet.api.model.Message m) {
        return (m != null && m.getDst() != null
                && m.getDst().equalsIgnoreCase(CustomCache.getDefaultUsername()))
                || m.getDst() == null; // BYPASS PQ O SERVER DO PROFESSOR MANDA DST == NULL
    }

    @Override
    public void onReceiveLogoffResponse() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(DirectMessagesActivity.this.getApplicationContext(), "Logoff feito com sucesso.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onReceiveMembersResponse(List<String> list) {
        // Stub
    }

    private void loadCache() {
        List<br.edu.infnet.api.model.Message> messageLog = CustomCache.getMessageHistory();

        if (messageLog != null) {
            Resposta r = new Resposta();
            r.setData(messageLog);

            onReceiveRespostaResponse(r);
        }
    }
}
