package br.edu.infnet.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.Socket;
import java.util.List;

import br.edu.infnet.api.Api;
import br.edu.infnet.api.interfaces.OnServerReceive;
import br.edu.infnet.api.model.Requisicao;
import br.edu.infnet.api.model.Resposta;
import br.edu.infnet.custom.CustomCache;
import br.edu.infnet.infnet.R;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements OnServerReceive {

    public static final String EXTRA_DATA = "bundle_data";
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mLoginView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Resposta resposta;
    private String sentData;
    private String receivedData;
    private boolean reconnect;
    private EditText mIpView;
    private EditText mPortView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CustomCache.getDefaultUsername() != null
                && !CustomCache.getDefaultUsername().isEmpty()) {

            Intent intent = new Intent(this, WindowSelectorActivity.class);
            startActivity(intent);
            finish();
//            Api.getInstance().getServerHistory();
        } else {
            CustomCache.destroyAll();

            setContentView(R.layout.activity_login);
            // Set up the login form.
            mLoginView = (AutoCompleteTextView) findViewById(R.id.username);
            mPasswordView = (EditText) findViewById(R.id.password);
            mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == R.id.login || id == EditorInfo.IME_NULL) {
                        attemptLogin();
                        return true;
                    }
                    return false;
                }
            });

            mIpView = (EditText) findViewById(R.id.ip);
            mPortView = (EditText) findViewById(R.id.port);

            Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
            mEmailSignInButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptLogin();
                }
            });

            mLoginFormView = findViewById(R.id.login_form);
            mProgressView = findViewById(R.id.login_progress);

            setDefaultFieldValues();
        }
    }

    private void setDefaultFieldValues() {
        mLoginView.setText(CustomCache.getDefaultUsername());
        mIpView.setText(CustomCache.getDefaultEndpoint());
        mPortView.setText(CustomCache.getDefaultPort());
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        mLoginView.setError(null);
        mIpView.setError(null);
        mPortView.setError(null);
        String login = mLoginView.getText().toString();
        String ip = mIpView.getText().toString();
        String port = mPortView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(login)) {
            mLoginView.setError(getString(R.string.error_field_required));
            focusView = mLoginView;
            cancel = true;
        } else if (!isEmailValid(login)) {
            mLoginView.setError(getString(R.string.error_invalid_email));
            focusView = mLoginView;
            cancel = true;
        }

        if (TextUtils.isEmpty(ip)) {
            mIpView.setError(getString(R.string.error_field_required));
            focusView = mIpView;
            cancel = true;
        } else if (!isEmailValid(ip)) {
            mIpView.setError(getString(R.string.error_invalid_email));
            focusView = mIpView;
            cancel = true;
        }

        if (TextUtils.isEmpty(port)) {
            mPortView.setError(getString(R.string.error_field_required));
            focusView = mLoginView;
            cancel = true;
        } else if (!isEmailValid(port)) {
            mPortView.setError(getString(R.string.error_invalid_email));
            focusView = mPortView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            mAuthTask = new UserLoginTask(login, null, ip, port);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {return true;}

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onAcceptSocket(Socket s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Connection accepted.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDataSend(final String data) {
        sentData = data;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), ">>> " + data, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onReceiveData(final String data) {
        receivedData = data;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "<<< " + data, Toast.LENGTH_SHORT).show();
            }
        });

        Intent intent = new Intent(this, WindowSelectorActivity.class);
        intent.putExtra(EXTRA_DATA, sentData);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSocketError(final Exception io) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), io.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onReceiveRequisicaoResponse(Requisicao r) {

    }

    @Override
    public void onReceiveRespostaResponse(Resposta r) {

    }

    @Override
    public void onReceiveLogoffResponse() {

    }

    @Override
    public void onReceiveMembersResponse(List<String> list) {
        // Stub!
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mLogin;
        private final String mIp;
        private final String mPort;

        UserLoginTask(String email, @Nullable String password, String ip, String port) {
            mLogin = email;
            mIp = ip;
            mPort = port;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            CustomCache.storeDefaultUsername(mLogin);
            CustomCache.storeDefaultEndpoint(mIp);
            CustomCache.storeDefaultPort(mPort);

            Api api = Api.getInstance();
            api.setListener(LoginActivity.this);
            api.setLogin(mLogin);
            api.setReconnect(false);
            api.setEndpoint(mIp);
            api.setDefaultPort(mPort);
            api.login(mLogin, null);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

