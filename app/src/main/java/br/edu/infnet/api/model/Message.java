package br.edu.infnet.api.model;

public class Message {
	private String dst;
	private String src;
	private String data;

	public String getSrc() {
		return src;
	}
	public String getData() {
		return data;
	}
	public String getDst() {
		return dst;
	}

	public Message(String src, String data, String dst) {
		this.src = src;
		this.data = data;
		this.dst = dst;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Message)) return false;

		Message message = (Message) o;

		if (getDst() != null ? !getDst().equals(message.getDst()) : message.getDst() != null)
			return false;
		if (getSrc() != null ? !getSrc().equals(message.getSrc()) : message.getSrc() != null)
			return false;
		return getData() != null ? getData().equals(message.getData()) : message.getData() == null;

	}

	@Override
	public int hashCode() {
		int result = getDst() != null ? getDst().hashCode() : 0;
		result = 31 * result + (getSrc() != null ? getSrc().hashCode() : 0);
		result = 31 * result + (getData() != null ? getData().hashCode() : 0);
		return result;
	}
}

