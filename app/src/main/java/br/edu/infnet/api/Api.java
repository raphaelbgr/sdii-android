package br.edu.infnet.api;

import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import br.edu.infnet.api.interfaces.OnServerReceive;
import br.edu.infnet.api.model.Message;
import br.edu.infnet.api.model.Requisicao;
import br.edu.infnet.api.model.Resposta;
import br.edu.infnet.custom.CustomCache;

/**
 * Created by Raphael on 14/02/2017.
 */

public class Api implements Runnable {

    private static boolean SHUTDOWN_SIGNAL = false;
    private Socket s = null;
    public static Api instance;
    private boolean IS_RUNNING = false;
    private boolean IS_HISTORY_RUNNING = false;
    private OnServerReceive listener;
    private boolean reconnect;
    private String storedLogin;
    private String defaultIp;
    private String defaultPort;


    public void sendMessage(final String data, final String dst) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (establishConnection() != null && s.isConnected()) {
                    Requisicao r = new Requisicao("enviar");
                    r.setId(storedLogin);
                    r.setMsgNr(0);
                    r.setDst(dst);
                    r.setData(data);
                    sendJson(s, r);
                } else {
                    // Call ApiError
                    listener.onSocketError(new Exception("Socket não está conectado."));
                }
            }
        }).start();
    }

    public void killGetHistoryThread() {
        this.IS_HISTORY_RUNNING = false;
    }

    public void getServerHistory() {

        final Runnable r1 = new Runnable() {
            @Override
            public void run() {

                try {
                    if (establishConnection() != null && s.isConnected()) {
                        Requisicao r = new Requisicao("receber");
                        r.setId(storedLogin);
                        r.setMsgNr(0);
                        sendJson(s, r);
                    } else {
                        // Call ApiError
                        listener.onSocketError(new Exception("Socket não está conectado."));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        if (!IS_HISTORY_RUNNING) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    IS_HISTORY_RUNNING = true;
                    while (IS_HISTORY_RUNNING && !SHUTDOWN_SIGNAL) {
                        new Thread(r1).start();
                        delay(2000);
                    }
                    killGetHistoryThread();
                }
            }).start();
        } else {
            IS_HISTORY_RUNNING = true;
        }
    }

    public void login(String login, @Nullable String password) {
        storedLogin = login;
        SHUTDOWN_SIGNAL = false;
        if (establishConnection() != null && s.isConnected()) {
            Requisicao r = buildLoginCommand("login", login, 0);
            sendJson(s, r);
        } else {
            // Call ApiError
            listener.onSocketError(new Exception("Socket não está conectado."));
        }
    }

    public void logoff() {
        if (establishConnection() != null && s.isConnected()) {
            Requisicao r = buildLoginCommand("logoff", storedLogin, 0);
            sendJson(s, r);
            dumpSocket(null);
            listener.onReceiveLogoffResponse();
        } else {
            // Call ApiError
            listener.onSocketError(new Exception("Socket não está conectado."));
        }
        this.reconnect = false;
        SHUTDOWN_SIGNAL = true;
        killGetHistoryThread();
    }

    private Requisicao buildLoginCommand(String cmd, String id, int msgNr) {
        Requisicao r = new Requisicao(cmd);
        r.setId(id);
        r.setMsgNr(msgNr);
        return r;
    }

    private void sendJson(Socket s, Requisicao r) {
        try {
            String json = new Gson().toJson(r);
            OutputStreamWriter out = null;
            out = new OutputStreamWriter(s.getOutputStream());
            out.write(json + "\n");
            out.flush();
            listener.onDataSend(json);
            Log.d("SND", json);
        } catch (Exception e) {
            e.printStackTrace();
            s = null;
            listener.onSocketError(e);
        }
    }

    private Socket establishConnection() {
        if (s == null || !s.isConnected()) {
            try {
                // Conecta com o server
                s = new Socket();
                s.connect(new InetSocketAddress(defaultIp, Integer.parseInt(defaultPort)), 7000);
                if (!s.isConnected())
                    throw new Exception("Socket timeout.");
                if (!IS_RUNNING)
                    new Thread(this).start();
                listener.onAcceptSocket(s);
            } catch (Exception e) {
                // Em caso de falha avisa à tela, dezfaz o socket
                // e espera 2000ms antes de entrar no procedimento de reconexão.
                e.printStackTrace();
                listener.onSocketError(e);
                dumpSocket(null);
                delay(3000);
                if (reconnect)
                    login(storedLogin, null);
            }
        }
        return s;
    }

    @Override
    public void run() {
        while (!SHUTDOWN_SIGNAL) {
            IS_RUNNING = true;
            BufferedReader br = null;
            String json = null;
            try {
                if (s == null)
                    throw new Exception("Sem conexão, aguardando reconexão...");
                else if (s.isConnected()) {
                    br = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    json = br.readLine();
                    if (json == null) {
                        throw new Exception("Sem dados do servidor");
                    } else {
                        Log.d("REC", json);
                        listener.onReceiveData(json);
                        callRequisicaoOrResposta(json);
                    }
                } else
                    throw new Exception("Conexão está fechada. Reconectando...");
            } catch (Exception e) {
                e.printStackTrace();
                listener.onSocketError(e);
                if (s != null)
                    dumpSocket(br);
                delay(500);
                if (reconnect)
                    login(storedLogin, null);
                IS_RUNNING = false;
            }
        } if (SHUTDOWN_SIGNAL)
            instance = null;
    }

    private void callRequisicaoOrResposta(String data) {
        try {
            Requisicao r = new Gson().fromJson(data, Requisicao.class);
            listener.onReceiveRequisicaoResponse(r);
        } catch (Exception e) {
            Resposta r = new Gson().fromJson(data, Resposta.class);
            listener.onReceiveRespostaResponse(r);
            listener.onReceiveMembersResponse(extractUserList(r.getData()));
        }
    }

    private void delay(long i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void dumpSocket(@Nullable BufferedReader br) {
        if (s != null) {
            try {
                s.close();
                if (br != null)
                    br.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                s = null;
            }
        }
    }

    private Api(){}
    public static Api getInstance() {
        if (instance == null) instance = new Api();
        return instance;
    }

    public void setListener(OnServerReceive listener) {
        this.listener = listener;
    }

    public void setReconnect(boolean reconnect) {
        this.reconnect = reconnect;
    }

    public void setLogin(String login) {
        this.storedLogin = login;
    }

    public void setEndpoint(String defaultIp) {
        this.defaultIp = defaultIp;
    }

    public void setDefaultPort(String defaultPort) {
        this.defaultPort = defaultPort;
    }

    public void loginAsync(final String login, final @Nullable String password) {
        new Thread(new Runnable() { @Override public void run() { login(login, password); } }).start();
    }

    private List<String> extractUserList(List<Message> data) {
        if (data != null) {
            List<String> clients = new LinkedList<>(CustomCache.getKnownUsers());
            for (Message m : data) {
                clients.add(m.getSrc());
            }
            Set<String> hashSet = new HashSet<>(clients);
            String [] temp = hashSet.toArray(new String[hashSet.size()]);
            List<String> tempList = Arrays.asList(temp);

            CustomCache.updateKnownUsers(tempList);

            return tempList;
        } else
            return CustomCache.getKnownUsers();
    }

    public void addKnownUser(String id) {
        List<String> temp = new LinkedList<>(CustomCache.getKnownUsers());
        Set<String> hashSet = new HashSet<>(temp);
        hashSet.add(id);
        String [] temp2 = hashSet.toArray(new String[hashSet.size()]);
        List<String> tempList = Arrays.asList(temp2);

        CustomCache.updateKnownUsers(tempList);

        listener.onReceiveMembersResponse(temp);
    }
}
