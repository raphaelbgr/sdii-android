package br.edu.infnet.api.interfaces;

/**
 * Created by raphael.bernardo on 15/02/2017.
 */

import java.net.Socket;
import java.util.List;

import br.edu.infnet.api.model.Requisicao;
import br.edu.infnet.api.model.Resposta;


public interface OnServerReceive {
    void onAcceptSocket(Socket s);
    void onDataSend(String data);
    void onReceiveData(String data);
    void onSocketError(Exception io);
    void onReceiveRequisicaoResponse(Requisicao r);
    void onReceiveRespostaResponse(Resposta r);
    void onReceiveLogoffResponse();
    void onReceiveMembersResponse(List<String> list);
}
